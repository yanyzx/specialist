/*
 * Appointments Messages
 *
 * This contains all the text for the Appointments component.
 */

import LocalizedStrings from 'react-localization';

export default new LocalizedStrings({
  en: {
    services: 'Services',
    contact: 'Contact',
    back: 'Back',
    principalTitle: 'Digital product specialist.',
    moreInfo: 'More info',
    growth: {
      title: 'Growth',
      description:
        'We follow and obtain data from your app, to create a growth strategy and usability improvements.',
      strategy: 'Growth Strategy',
      strategyDescription: 'Recommendations about user experience based on:',
      strategyList: ['Data & Analitycs', 'Usability Test', 'User Research'],
      activation: 'Activation Boost',
      activationDescription: 'Re design and user experience testing.',
      activationList: [
        'Data & Analitycs',
        'Strategy',
        'Usability Test',
        'User Research',
        'UI/UX Design',
      ],
    },
    creation: {
      title: 'Creation',
      description:
        'We offer Specialized services to design and prototype your new app.',
      product: 'Product Strategy',
      productDescription:
        'Design and analysis to cover all the needs of your new app',
      productList: [
        'Design Thinking',
        'User Research',
        'UI/UX Design',
        'Usability Test',
      ],
      designSprint: 'Design Sprint',
      designSprintDescription:
        'Design Sprint is a five-day process for answering critical business questions through design, prototyping, and testing ideas with final users and customers, all this from our friends in Google.',
    },
    featuredClients: 'Featured Clients',
    legend:
      'The essence of what we do lives in the need to keep looking forward, keep innovating through design, using the right methods  for answering critical business questions and improving the experience of different users.',
    footer: {
      title: 'We design, prototype and grow your app.',
      askWork: 'Want us to work with you?',
      email: 'hello@submarine.app',
      number: '6677215815',
      newsletter:
        'Newsletter. Digital design, user experience, user research, usability test.',
      yourEmailAddress: 'Your email address',
      send: 'Send',
      contact: 'Contact',
      submarine: 'Submarine',
    },
  },
  es: {
    services: 'Servicios',
    contact: 'Contacto',
    back: 'Regresar',
    principalTitle: 'Digital product specialist.',
    moreInfo: 'Mas información',
    growth: {
      title: 'Crecimiento',
      description:
        'Monitoreamos tu aplicación con el fin de obtener datos que nos ayuden a crear estrategías de crecimiento y mejoras de usabilidad.',
      strategy: 'Estrategia de crecimiento',
      strategyDescription: 'Recomendaciones sobre experiencia de usuario:',
      strategyList: [
        'Análisis de datos',
        'Pruebas de usabilidad',
        'Investigación de usuario',
      ],
      activation: 'Activación',
      activationDescription:
        'Rediseño y pruebas de experiencia con tu usuario.',
      activationList: [
        'Análisis de datos',
        'Estrategia',
        'Pruebas de usabilidad',
        'Investigación de usuario',
        'UI/UX Diseño',
      ],
    },
    creation: {
      title: 'Creación',
      description:
        'Te ofrecemos servicios especializados con los cuales te ayudamos a diseñar y prototipar tu aplicación.',
      product: 'Estrategia de producto',
      productDescription:
        'Diseño de aplicaciones a la medida de tus necesidades.',
      productList: [
        'Design Thinking',
        'Investigación de usuario',
        'UI/UX Diseño',
        'Pruebas de usabilidad',
      ],
      designSprint: 'Design Sprint',
      designSprintDescription:
        'Design Sprint es una metodología que permite prototipar y validar ideas con usuarios finales de manera rápida, con el fin de definir el roadmap de un producto en 5 fases.',
    },
    featuredClients: 'Clientes destacados',
    legend:
      'La esencia de lo que hacemos está en la necesidad de seguir innovando a través del diseño y los métodos más acertados para resolver dilemas de negocio mejorando ante todo la experiencia de los diferentes usuarios.',
    footer: {
      title: 'Diseñamos, prototipamos y hacemos crecer su aplicación.',
      askWork: '¿Quieres que trabajemos contigo?',
      email: 'hello@submarine.app',
      number: '6677215815',
      newsletter:
        'Newsletter.  Digital design, user experience, user research, usability test.',
      yourEmailAddress: 'Tú dirección de correo',
      send: 'Enviar',
      contact: 'Contacto',
      submarine: 'Submarine',
    },
  },
});
