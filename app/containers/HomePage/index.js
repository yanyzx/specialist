import React from 'react';

import Section from 'components/Section';
import Coso from 'components/Coso';
import Client from 'components/Client';
import Topbar from 'components/Topbar';

import Background from './imgs/background.svg';
import BackgroundMobile from './imgs/background-mobile.svg';
import Essence from './imgs/essence.jpg';
import Product1 from './imgs/product-1.svg';
import Product2 from './imgs/product-2.svg';
import Product3 from './imgs/product-3.svg';
import Product4 from './imgs/product-4.svg';
import Client1 from './imgs/client-1.svg';
import Client2 from './imgs/client-2.svg';
import Client3 from './imgs/client-3.svg';
import Client4 from './imgs/client-4.svg';
import Client5 from './imgs/client-5.svg';
import Client6 from './imgs/client-6.svg';

import copys from './messages';

import {
  HomePageContainer,
  HeaderContainer,
  Image,
  ImageMobile,
  Title,
  ContentContainer,
  SectionContainer,
  CosoContainer,
  ClientsTitle,
  ClientContainer,
  MessageContainer,
  MessageImage,
  MessageText,
} from './styles';

export class HomePage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <HomePageContainer>
        <HeaderContainer>
          <Topbar />
          <Title>{copys.principalTitle}</Title>
          <Image src={Background} alt="Background" />
          <ImageMobile src={BackgroundMobile} alt="Background" />
        </HeaderContainer>

        <ContentContainer>
          <SectionContainer>
            <Section
              title={copys.growth.title}
              description={copys.growth.description}
              color="#ffb74d"
            />
          </SectionContainer>

          <CosoContainer>
            <Coso
              image={Product1}
              title={copys.growth.strategy}
              description={copys.growth.strategyDescription}
              list={copys.growth.strategyList}
              btnText={copys.moreInfo}
              url="/growth-strategy"
            />
          </CosoContainer>
          <CosoContainer>
            <Coso
              image={Product2}
              title={copys.growth.activation}
              description={copys.growth.activationDescription}
              list={copys.growth.activationList}
              btnText={copys.moreInfo}
              url="/activation"
            />
          </CosoContainer>

          <SectionContainer>
            <Section
              title={copys.creation.title}
              description={copys.creation.description}
              color="#fa7866"
            />
          </SectionContainer>

          <CosoContainer>
            <Coso
              image={Product3}
              title={copys.creation.product}
              description={copys.creation.productDescription}
              list={copys.creation.productList}
              btnText={copys.moreInfo}
              url="/product-strategy"
            />
          </CosoContainer>
          <CosoContainer>
            <Coso
              image={Product4}
              title={copys.creation.designSprint}
              description={copys.creation.designSprintDescription}
              btnText={copys.moreInfo}
              url="/design-sprint"
            />
          </CosoContainer>

          <ClientsTitle>{copys.featuredClients}</ClientsTitle>

          <ClientContainer>
            <Client image={Client1} />
          </ClientContainer>
          <ClientContainer>
            <Client image={Client2} />
          </ClientContainer>
          <ClientContainer>
            <Client image={Client3} />
          </ClientContainer>
          <ClientContainer>
            <Client image={Client4} />
          </ClientContainer>
          <ClientContainer>
            <Client image={Client5} />
          </ClientContainer>
          <ClientContainer>
            <Client image={Client6} />
          </ClientContainer>
        </ContentContainer>

        <MessageContainer>
          <MessageImage src={Essence} />
          <MessageText>{copys.legend}</MessageText>
        </MessageContainer>
      </HomePageContainer>
    );
  }
}

export default HomePage;
