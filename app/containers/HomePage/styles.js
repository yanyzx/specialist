import styled from 'styled-components';

export const HomePageContainer = styled.div`
  float: left;
  width: 100%;
`;

export const HeaderContainer = styled.div`
  height: 100vh;
  width: 100%;
  position: relative;
`;

export const Image = styled.img`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
  object-fit: cover;
  @media (max-width: 576px) {
    display: none;
  }
`;

export const ImageMobile = styled.img`
  display: none;
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
  object-fit: cover;
  @media (max-width: 576px) {
    display: initial;
  }
`;

export const Title = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  text-align: center;
  font-weight: 600;
  font-size: 65px;
  letter-spacing: 2px;
  color: rgba(255, 255, 255, 0.85);
  @media (max-width: 576px) {
    left: 16px;
    font-size: 50px;
    transform: translate(0, -50%);
    text-align: left;
  }
`;

export const ContentContainer = styled.div`
  float: left;
  width: 100%;
  padding: 80px 10%;
`;

export const SectionContainer = styled.div`
  float: left;
  width: 100%;
  padding-bottom: 80px;
`;

export const CosoContainer = styled.div`
  float: left;
  width: 50%;
  padding-bottom: 120px;
  @media (max-width: 576px) {
    width: 100%;
  }
`;

export const ClientsTitle = styled.div`
  float: left;
  width: 100%;
  font-size: 36px;
  font-weight: 500;
  color: #424242;
  padding-bottom: 80px;
`;

export const ClientContainer = styled.div`
  float: left;
  width: 33%;
  padding: 1%;
  @media (max-width: 576px) {
    width: 50%;
  }
`;

export const MessageContainer = styled.div`
  float: left;
  width: 100%;
  position: relative;
  background-color: #fafafa;
  padding: 80px 10%;
  @media (max-width: 576px) {
    padding: 80px 0;
  }
`;

export const MessageImage = styled.img`
  width: 66%;
  height: 100%;
  max-height: 650px;
  object-fit: cover;
`;

export const MessageText = styled.div`
  position: absolute;
  font-size: 20px;
  bottom: 16%;
  right: 10%;
  width: 33%;
  padding: 32px;
  background-color: #d5fee1;
  @media (max-width: 576px) {
    right: 0;
    bottom: 8%;
    font-size: 10px;
    width: 50%;
    padding: 16px;
  }
`;
