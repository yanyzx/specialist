/*
 * Appointments Messages
 *
 * This contains all the text for the Appointments component.
 */

import LocalizedStrings from 'react-localization';

export default new LocalizedStrings({
  en: {
    principalTitle: 'Activation, ',
    principalDescription: 'level up the  user experience in your app.',
    moreInfo: 'Start Now',
    sprint: {
      title: 'Experience',
      description:
        'We focus in usability improvement on your app, with the only purpose to increase productivity to the top.',
      listTitle: 'Disciplines:',
      list: [
        'Data & Analitycs',
        'Strategy',
        'Usability test',
        'User research',
        'UI/UX Design',
      ],
    },
    benefit: {
      title: 'How can you benefit from it?',
      description:
        'Knowing the lowpoint of your app, is one of the most valuable facts to know, it gives you the chance to change your weaknesses into strength points.',
    },
    action: {
      title: 'Wanna contact us?',
      btn: 'Get more info',
    },
  },
  es: {
    principalTitle: 'Activation, ',
    principalDescription:
      'mejora la experiencia de tus usuarios en tu aplicación.',
    moreInfo: 'Inicia ya',
    sprint: {
      title: 'Experience',
      description:
        'Nos enfocamos en mejorar la la usabilidad de tu aplicación, con el fin de lograr mayores conversiones en la misma.',
      listTitle: 'Disciplinas empleadas:',
      list: [
        'Analisis de datos',
        'Estrategia',
        'Pruebas de usabilidad',
        'Investigación de usuario',
        'UI/UX Diseño',
      ],
    },
    benefit: {
      title: '¿Cómo me beneficia este servicio?',
      description:
        'Conocer los puntos de dolor de tu aplicación y darles solucion es de suma importancia, ya que así más usuarios lograran generar más conversiones.',
    },
    action: {
      title: '¿Quiere contactarnos?',
      btn: 'Obtener más información',
    },
  },
});
