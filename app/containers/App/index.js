/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage';
import DesignSprint from 'containers/DesignSprint';
import GrowthStrategy from 'containers/GrowthStrategy';
import Activation from 'containers/Activation';
import ProductStrategy from 'containers/ProductStrategy';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Footer from 'components/Footer';

const AppWrapper = styled.div``;

export default function App() {
  return (
    <AppWrapper>
      <Helmet titleTemplate="%s - Submarine" defaultTitle="Submarine">
        <meta name="description" content="Submarine application" />
      </Helmet>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/design-sprint" component={DesignSprint} />
        <Route path="/growth-strategy" component={GrowthStrategy} />
        <Route path="/activation" component={Activation} />
        <Route path="/product-strategy" component={ProductStrategy} />
        <Route path="" component={NotFoundPage} />
      </Switch>
      <Footer />
    </AppWrapper>
  );
}
