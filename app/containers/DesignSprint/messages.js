/*
 * Appointments Messages
 *
 * This contains all the text for the Appointments component.
 */

import LocalizedStrings from 'react-localization';

export default new LocalizedStrings({
  en: {
    principalTitle: 'Design Sprint, ',
    principalDescription: '5 days, design, prototype and prove ideas.',
    moreInfo: 'Start Now $59,900.00 MXN',
    sprint: {
      title: 'Sprint',
      description:
        'Design Sprint is a great method that allow us to prototype and prove our ideas with final users fast and easy, this si how we build the roadmap of a product in 5 fases.',
    },
    benefit: {
      title: 'How can you benefit from it?',
      description:
        'It is just a solid win, Design Sprint was created to work fast and intense for getting the right solution to a business problem through design. It has all the benefits from low cost to saving time, you can also define a plan of validation using feedback from the users.',
    },
    action: {
      title: 'Wanna contact us?',
      btn: 'Get more info',
    },
  },
  es: {
    principalTitle: 'Design Sprint, ',
    principalDescription: '5 días, diseña, prototipa y valida ideas.',
    moreInfo: 'Desde $59,900.00 MXN',
    sprint: {
      title: 'Sprint',
      description:
        'Design Sprint es una metodología que permite prototipar y validar ideas con usuarios finales de manera rápida, con el fin de definir el roadmap de un producto en 5 fases.',
    },
    benefit: {
      title: '¿Cómo me beneficia este servicio?',
      description:
        'Los Design Sprint están pensados para trabajar de forma rápida e intensa en conseguir una solución a un problema de negocio mediante el diseño. Este es un súper beneficio porque ahorras mucho tiempo y dinero y te permite definir un plan de validación basado en el feedback de tus usuarios.',
    },
    action: {
      title: '¿Quiere contactarnos?',
      btn: 'Obtener más información',
    },
  },
});
