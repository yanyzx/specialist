import React from 'react';

import Topbar from 'components/Topbar';
import Article from 'components/Article';
import ActionCall from 'components/ActionCall';

import Logo from './imgs/logo.svg';
import Background from './imgs/background.svg';
import BackgroundMobile from './imgs/background-mobile.svg';
import Characteristic1Img from './imgs/characteristic-1.svg';
import Characteristic2Img from './imgs/characteristic-2.svg';
import CharacteristicMobileImg from './imgs/characteristic-mobile.svg';

import copys from './messages';

import {
  WrapperContainer,
  HeaderContainer,
  Image,
  ImageMobile,
  TitleContainer,
  HeaderTitle,
  HeaderBtn,
  ContentContainer,
  CommonContainer,
  TopicContainer,
  TopicImageContainer,
  MobileImageContainer,
  BeneficImageContainer,
  BeneficContainer,
  CharacteristicImage,
  ActionCallContainer,
} from './styles';

export class DesignSprint extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <WrapperContainer>
        <HeaderContainer>
          <Topbar logo={Logo} color="#304ffe" back />
          <TitleContainer>
            <HeaderTitle>
              <span style={{ color: '#2962ff' }}>{copys.principalTitle}</span>
              {copys.principalDescription}
            </HeaderTitle>
            <HeaderBtn to="/">{copys.moreInfo}</HeaderBtn>
          </TitleContainer>
          <Image src={Background} alt="Background" />
          <ImageMobile src={BackgroundMobile} alt="Background" />
        </HeaderContainer>

        <ContentContainer>
          <CommonContainer>
            <TopicContainer>
              <Article
                title={copys.sprint.title}
                description={copys.sprint.description}
              />
            </TopicContainer>

            <TopicImageContainer>
              <CharacteristicImage src={Characteristic1Img} />
            </TopicImageContainer>
          </CommonContainer>

          <MobileImageContainer>
            <CharacteristicImage src={CharacteristicMobileImg} />
          </MobileImageContainer>

          <CommonContainer>
            <BeneficImageContainer>
              <CharacteristicImage src={Characteristic2Img} />
            </BeneficImageContainer>

            <BeneficContainer>
              <Article
                title={copys.benefit.title}
                description={copys.benefit.description}
              />
            </BeneficContainer>
          </CommonContainer>

          <ActionCallContainer>
            <ActionCall title={copys.action.title} btnText={copys.action.btn} />
          </ActionCallContainer>
        </ContentContainer>
      </WrapperContainer>
    );
  }
}

export default DesignSprint;
