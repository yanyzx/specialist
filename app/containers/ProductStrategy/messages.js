/*
 * Appointments Messages
 *
 * This contains all the text for the Appointments component.
 */

import LocalizedStrings from 'react-localization';

export default new LocalizedStrings({
  en: {
    principalTitle: 'Product Strategy, ',
    principalDescription: 'Analysis, design and usability test.',
    moreInfo: 'Start Now $114,900.00 MXN',
    sprint: {
      title: 'Strategy',
      description:
        'We work in app design according to your needs, working hand by hand with you.',
      listTitle: 'Disciplines:',
      list: [
        'Design Thinking',
        'User research',
        'UI/UX Design',
        'Usability test',
      ],
    },
    benefit: {
      title: 'How can you benefit from it?',
      description:
        "Let's get real. Starting a development process its expensive, even if it is just a new function. That's why its important to design solutions, prototype and analyze to help us validate our ideas.",
    },
    action: {
      title: 'Wanna contact us?',
      btn: 'Get more info',
    },
  },
  es: {
    principalTitle: 'Estrategia de producto, ',
    principalDescription:
      'analisis, diseño, usabilidad y pruebas con usuarios.',
    moreInfo: 'Inicia ya por $114,900.00 MXN',
    sprint: {
      title: 'Estrategia',
      description:
        'Diseñamos aplicaciones a la medida de tus necesidades, trabajando mano a mano contigo y tu equipo.',
      listTitle: 'Disciplinas empleadas:',
      list: [
        'Design Thinking',
        'Investigación de usuario',
        'UI/UX Diseño',
        'Pruebas de usabilidad',
      ],
    },
    benefit: {
      title: '¿Cómo me beneficia este servicio?',
      description:
        'Antes de invertir en el desarrollo de tu producto o de una nueva funcionalidad que requiere un proceso costoso, es importante diseñar soluciones, crear un prototipo funcional y validar nuestras ideas.',
    },
    action: {
      title: '¿Quiere contactarnos?',
      btn: 'Obtener más información',
    },
  },
});
