/*
 * Appointments Messages
 *
 * This contains all the text for the Appointments component.
 */

import LocalizedStrings from 'react-localization';

export default new LocalizedStrings({
  en: {
    principalTitle: 'Growth strategy, ',
    principalDescription: 'know your users and how they interact in your app.',
    moreInfo: 'Start Now',
    sprint: {
      title: 'Insights',
      description:
        'Interviews with users, monitoring the app, to get data of who an how they interact inside your app.',
      listTitle: 'Disciplines:',
      list: ['Data & Analitycs', 'Usability test', 'User research'],
    },
    benefit: {
      title: 'How can you benefit from it?',
      description:
        "We get deep down to know exactly who the user is and how they react to  your app. To know this is one of the most important things to help you out taking decisions of the path that you're taking or the future that you really want.",
    },
    action: {
      title: 'Wanna contact us?',
      btn: 'Get more info',
    },
  },
  es: {
    principalTitle: 'Estrategia de crecimiento, ',
    principalDescription: 'conoce a tus usuarios y como interactuan en tu app.',
    moreInfo: 'Inicia ya',
    sprint: {
      title: 'Datos',
      description:
        'Monitoreamos tu aplicación y relizamos entrevistas con tu usuario para recabar datos de quien y como interactua con tu aplicación.',
      listTitle: 'Disciplinas empleadas:',
      list: [
        'Analisis de datos',
        'Pruebas de usabilidad',
        'Investigación de usuario',
      ],
    },
    benefit: {
      title: '¿Cómo me beneficia este servicio?',
      description:
        'Nos sumergimos para conocer quien es tu usuario y como se comporta dentro de tu aplicación. Conocer esta información es de suma importancia, ya que te ayuda a tomar decisiones sobre el camino o el futuro de tu producto digital.',
    },
    action: {
      title: '¿Quiere contactarnos?',
      btn: 'Obtener más información',
    },
  },
});
