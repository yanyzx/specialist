import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const WrapperContainer = styled.div`
  float: left;
  width: 100%;
`;

export const HeaderContainer = styled.div`
  height: 100vh;
  width: 100%;
  position: relative;
`;

export const Image = styled.img`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
  object-fit: cover;
  @media (max-width: 576px) {
    display: none;
  }
`;

export const ImageMobile = styled.img`
  display: none;
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
  object-fit: cover;
  @media (max-width: 576px) {
    display: initial;
  }
`;

export const TitleContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 10%;
  transform: translate(0, -50%);
  z-index: 2;
`;

export const HeaderTitle = styled.div`
  width: 40%;
  font-weight: 600;
  font-size: 36px;
  letter-spacing: 2px;
  color: #424242;
  padding-bottom: 24px;
  @media (max-width: 576px) {
    font-size: 24px;
    width: 90%;
  }
`;

export const HeaderBtn = styled(Link)`
  text-decoration: none;
  height: 40px;
  border-radius: 7px;
  cursor: pointer;
  display: inline-block;
  padding: 0 32px;
  line-height: 40px;
  font-size: 14px;
  background-color: #2962ff;
  color: #fafafa;
`;

export const ContentContainer = styled.div`
  float: left;
  width: 100%;
  padding: 80px 10%;
`;

export const CommonContainer = styled.div`
  float: left;
  width: 100%;
`;

export const TopicContainer = styled.div`
  float: left;
  width: 50%;
  padding-right: 8%;
  padding-bottom: 120px;
  @media (max-width: 576px) {
    width: 100%;
    padding-right: 0;
  }
`;

export const TopicImageContainer = styled.div`
  float: left;
  width: 50%;
  padding-left: 8%;
  padding-top: 70px;
  padding-bottom: 120px;
  @media (max-width: 576px) {
    display: none !important;
  }
`;

export const MobileImageContainer = styled.div`
  display: none;
  float: left;
  width: 100%;
  padding-top: 70px;
  padding-bottom: 120px;
  @media (max-width: 576px) {
    display: initial;
  }
`;

export const BeneficImageContainer = styled.div`
  float: left;
  width: 50%;
  padding-right: 8%;
  padding-bottom: 120px;
  @media (max-width: 576px) {
    display: none;
  }
`;

export const BeneficContainer = styled.div`
  float: left;
  width: 50%;
  padding-left: 8%;
  padding-bottom: 120px;
  @media (max-width: 576px) {
    width: 100%;
    padding-left: 0;
  }
`;

export const CharacteristicImage = styled.img`
  width: 100%;
  height: 100%;
`;

export const ActionCallContainer = styled.div`
  float: left;
  width: 100%;
  padding: 80px;
  @media (max-width: 576px) {
    padding: 0;
  }
`;
