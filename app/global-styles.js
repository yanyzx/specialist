import { injectGlobal } from 'styled-components';
import CircularStdOTF from './fonts/CircularStd/CircularStd-Book.otf';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  @font-face {
    font-family: 'CircularStd';
    src: url('${CircularStdOTF}');
    font-weight: normal;
    font-style: normal;
  }

  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'CircularStd', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'CircularStd', 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
`;
