import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const ActionCallContainer = styled.div`
  float: left;
  width: 100%;
`;

export const Title = styled.div`
  font-size: 36px;
  text-align: center;
  color: #424242;
  font-weight: bold;
  padding-bottom: 24px;
`;

export const BtnContainer = styled.div`
  float: left;
  text-align: center;
  width: 100%;
`;

export const Btn = styled(Link)`
  text-decoration: none;
  height: 40px;
  border-radius: 7px;
  cursor: pointer;
  display: inline-block;
  padding: 0 32px;
  line-height: 40px;
  font-size: 14px;
  background-color: #2962ff;
  color: #fafafa;
`;
