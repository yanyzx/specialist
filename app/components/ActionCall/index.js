import React from 'react';
import PropTypes from 'prop-types';

import { ActionCallContainer, Title, BtnContainer, Btn } from './styles';

export class ActionCall extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { title, btnText } = this.props;
    return (
      <ActionCallContainer>
        <Title>{title}</Title>
        <BtnContainer>
          <Btn to="/">{btnText}</Btn>
        </BtnContainer>
      </ActionCallContainer>
    );
  }
}

ActionCall.propTypes = {
  title: PropTypes.string,
  btnText: PropTypes.string,
};

export default ActionCall;
