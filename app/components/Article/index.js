import React from 'react';
import PropTypes from 'prop-types';

import {
  ArticleContainer,
  Title,
  Description,
  ListTitle,
  List,
} from './styles';

export class Article extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { title, description, listTitle, list } = this.props;

    const listComponent = list ? (
      <List>
        {list.map((item, index) => <div key={'item-' + index}>- {item}</div>)}
      </List>
    ) : null;

    return (
      <ArticleContainer>
        <Title>{title}</Title>
        <Description>{description}</Description>
        <ListTitle>{listTitle}</ListTitle>
        {listComponent}
      </ArticleContainer>
    );
  }
}

Article.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  listTitle: PropTypes.string,
  list: PropTypes.array,
};

export default Article;
