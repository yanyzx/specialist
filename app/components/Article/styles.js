import styled from 'styled-components';

export const ArticleContainer = styled.div`
  float: left;
  width: 100%;
`;

export const Title = styled.div`
  font-size: 36px;
  color: #424242;
  font-weight: bold;
  padding-bottom: 16px;
`;

export const Description = styled.div`
  font-size: 20px;
  font-family: 'Space Mono';
  color: #212121;
  text-align: justify;
  padding-bottom: 24px;
`;

export const ListTitle = styled.div`
  font-size: 20px;
  font-weight: 700;
  color: #424242;
  font-family: 'Space Mono';
  padding-bottom: 24px;
`;

export const List = styled.div`
  font-size: 20px;
  font-weight: 700;
  color: #424242;
  font-family: 'Space Mono';
  padding-bottom: 24px;
`;
