import styled from 'styled-components';

export const SectionContainer = styled.div`
  float: left;
  width: 100%;
`;

export const Title = styled.div`
  font-size: 36px;
  color: #424242;
  padding-bottom: 16px;
`;

export const Description = styled.div`
  font-size: 24px;
  color: #424242;
  padding-bottom: 24px;
`;

export const ColorLine = styled.div`
  background-color: ${props => props.color};
  width: 50px;
  height: 7px;
`;
