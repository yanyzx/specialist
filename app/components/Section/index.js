import React from 'react';
import PropTypes from 'prop-types';

import { SectionContainer, Title, Description, ColorLine } from './styles';

export class Section extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { title, description, color } = this.props;
    return (
      <SectionContainer>
        <Title>{title}</Title>
        <Description>{description}</Description>
        <ColorLine color={color} />
      </SectionContainer>
    );
  }
}

Section.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  color: PropTypes.string,
};

export default Section;
