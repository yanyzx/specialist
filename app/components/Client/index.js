import React from 'react';
import PropTypes from 'prop-types';

import { ClientContainer, Image } from './styles';

export class Client extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { image } = this.props;
    return (
      <ClientContainer>
        <Image src={image} alt="Image" />
      </ClientContainer>
    );
  }
}

Client.propTypes = {
  image: PropTypes.element,
};

export default Client;
