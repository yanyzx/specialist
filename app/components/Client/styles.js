import styled from 'styled-components';

export const ClientContainer = styled.div`
  float: left;
  position: relative;
  width: 100%;
  height: 184px;
  background-color: #fafafa;
  text-align: center;
`;

export const Image = styled.img`
  width: 66%;
  max-height: 60%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
