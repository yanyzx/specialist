import React from 'react';
import PropTypes from 'prop-types';

import DefaultLogoImg from './imgs/logo.svg';
import BackArrowImg from './imgs/back.svg';
import {
  TopbarContainer,
  BackLink,
  BackImage,
  ImageContainerLink,
  Image,
  LeftTitle,
  RightTitle,
} from './styles';

import copys from '../../containers/HomePage/messages';

export class Topbar extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { logo, color, back } = this.props;
    const leftBtnComponent = back ? (
      <BackLink to="/" color={color}>
        <BackImage src={BackArrowImg} />
        {copys.back}
      </BackLink>
    ) : (
      <LeftTitle color={color}>{copys.services}</LeftTitle>
    );

    return (
      <div>
        <TopbarContainer>
          {leftBtnComponent}
          <ImageContainerLink to="/" color={color}>
            <Image src={logo || DefaultLogoImg} alt="Submarine - Logo" />
          </ImageContainerLink>
          <RightTitle color={color}>{copys.contact}</RightTitle>
        </TopbarContainer>
      </div>
    );
  }
}

Topbar.propTypes = {
  logo: PropTypes.element,
  color: PropTypes.string,
  back: PropTypes.boolean,
};

export default Topbar;
