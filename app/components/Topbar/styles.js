import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const TopbarContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 50px;
  padding: 16px 2.5% 0 2.5%;
  text-align: center;
  z-index: 1000;
`;

export const ImageContainerLink = styled(Link)`
  text-align: center;
  height: 50px;
  width: 60px;
  display: inline-block;

  @media (max-width: 576px) {
    float: left;
  }
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
`;

export const BackLink = styled(Link)`
  text-decoration: none;
  float: left;
  line-height: 50px;
  font-size: 18px;
  color: ${props => props.color || '#fff'};
  cursor: pointer;
  @media (max-width: 576px) {
    display: none;
  }
`;

export const BackImage = styled.img`
  padding: 0 4px 6px 0;
`;

export const LeftTitle = styled.div`
  float: left;
  line-height: 50px;
  font-size: 18px;
  color: ${props => props.color || '#fff'};
  cursor: pointer;
  @media (max-width: 576px) {
    display: none;
  }
`;

export const RightTitle = styled.div`
  float: right;
  line-height: 50px;
  font-size: 18px;
  color: ${props => props.color || '#fff'};
  cursor: pointer;
  @media (max-width: 576px) {
    display: none;
  }
`;
