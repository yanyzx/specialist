import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const FooterContainer = styled.div`
  float: left;
  width: 100%;
  padding: 48px 10%;
  background-color: #213ee7;
`;

export const TextContainer = styled.div`
  float: left;
  width: 50%;
  @media (max-width: 576px) {
    width: 100%;
  }
`;

export const Title = styled.div`
  font-size: 24px;
  color: #fff;
  padding-bottom: 80px;
`;

export const Subtitle = styled.div`
  font-size: 20px;
  color: #fafafa;
  padding-bottom: 48px;
`;

export const Contact = styled.div`
  font-size: 18px;
  color: #fafafa;
  padding-bottom: 32px;
`;

export const LinkTo = styled.a`
  text-decoration: none;
  color: #fafafa;
`;

export const ContactBtn = styled(Link)`
  text-decoration: none;
  height: 40px;
  border-radius: 7px;
  cursor: pointer;
  display: inline-block;
  padding: 0 32px;
  line-height: 40px;
  font-size: 14px;
  background-color: #fff;
  color: #2962ff;
  margin-bottom: 88px;
`;

export const Newsletter = styled.div`
  font-size: 14px;
  color: #fafafa;
  padding-bottom: 48px;
`;

export const EmailInput = styled.input`
  width: 250px;
  border: none;
  border-radius: 3px;
  padding: 8px;
  font-size: 14px;
  background-color: #bbccfc;
  color: #304ffe;
  margin-bottom: 32px;
`;

export const NewsletterBtn = styled(Link)`
  text-decoration: none;
  height: 40px;
  border-radius: 7px;
  border: solid 2px #fff;
  cursor: pointer;
  display: inline-block;
  padding: 0 32px;
  line-height: 40px;
  font-size: 14px;
  background-color: #213ee7;
  color: #fff;
  margin-bottom: 88px;
`;

export const IconsContainer = styled.div`
  float: left;
  width: 100%;
  position: relative;
  text-align: center;
`;

export const Brand = styled.div`
  position: absolute;
  bottom: 0;
  font-size: 14px;
  color: #fff;
  @media (max-width: 576px) {
    position: initial;
    padding-bottom: 40px;
  }
`;

export const Icon = styled.img`
  margin: 0 32px;
  width: 20px;
  height: 20px;
`;
