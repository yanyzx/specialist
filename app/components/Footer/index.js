import React from 'react';

import {
  FooterContainer,
  TextContainer,
  Title,
  Subtitle,
  Contact,
  LinkTo,
  ContactBtn,
  Newsletter,
  EmailInput,
  NewsletterBtn,
  IconsContainer,
  Brand,
  Icon,
} from './styles';

import FacebookImg from './imgs/facebook.svg';
import MediumImg from './imgs/medium.svg';
import TwitterImg from './imgs/twitter.svg';

import copys from '../../containers/HomePage/messages';

export class Footer extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <FooterContainer>
        <Title>{copys.footer.title}</Title>
        <TextContainer>
          <Subtitle>{copys.footer.askWork}</Subtitle>
          <Contact>
            <LinkTo href={'mailto:' + copys.footer.email}>
              {copys.footer.email}
            </LinkTo>
            <br />
            <LinkTo href={'tel:' + copys.footer.number}>
              {copys.footer.number}
            </LinkTo>
          </Contact>
          <ContactBtn to="/">{copys.footer.contact}</ContactBtn>
        </TextContainer>
        <TextContainer>
          <Newsletter>{copys.footer.newsletter}</Newsletter>
          <EmailInput placeholder={copys.footer.yourEmailAddress} />
          <br />
          <NewsletterBtn to="/">{copys.footer.send}</NewsletterBtn>
        </TextContainer>
        <IconsContainer>
          <Brand>© {copys.footer.submarine} 2018</Brand>
          <Icon src={MediumImg} />
          <Icon src={TwitterImg} />
          <Icon src={FacebookImg} />
        </IconsContainer>
      </FooterContainer>
    );
  }
}

export default Footer;
