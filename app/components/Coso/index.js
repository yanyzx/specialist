import React from 'react';
import PropTypes from 'prop-types';

import { CosoContainer, Image, Title, Description, List, Btn } from './styles';

export class Coso extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { image, title, description, list, btnText, url } = this.props;

    const listComponent = list ? (
      <List>
        {list.map((item, index) => <div key={'item-' + index}>- {item}</div>)}
      </List>
    ) : null;

    return (
      <CosoContainer>
        <Image src={image} alt="Image" />
        <Title>{title}</Title>
        <Description>{description}</Description>
        {listComponent}
        <Btn to={url}>{btnText}</Btn>
      </CosoContainer>
    );
  }
}

Coso.propTypes = {
  image: PropTypes.element,
  title: PropTypes.string,
  description: PropTypes.string,
  list: PropTypes.array,
  btnText: PropTypes.string,
  url: PropTypes.string,
};

export default Coso;
