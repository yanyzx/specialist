import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const CosoContainer = styled.div`
  float: left;
  width: 100%;
`;

export const Image = styled.img`
  width: 45px;
  height: 45px;
  margin-bottom: 24px;
`;

export const Title = styled.div`
  font-size: 24px;
  color: #424242;
  padding-bottom: 24px;
`;

export const Description = styled.div`
  font-size: 18px;
  color: #212121;
  font-family: 'Space Mono';
  padding-bottom: 24px;
`;

export const List = styled.div`
  font-size: 18px;
  color: #212121;
  font-family: 'Space Mono';
  padding-bottom: 24px;
`;

export const Btn = styled(Link)`
  text-decoration: none;
  height: 40px;
  border-radius: 7px;
  border: solid 2px #2962ff;
  cursor: pointer;
  display: inline-block;
  padding: 0 32px;
  line-height: 36px;
  font-size: 14px;
  color: #2962ff;
`;
